/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.satit.oxoop;

/**
 *
 * @author satit
 */
public class Board {

    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player currentPlayer;
    private Player o;
    private Player x;
    private int count;
    private int row, col;
    public boolean Win = false;
    public boolean draw = false;

    public Board(Player o, Player x) {
        this.o = o;
        this.x = x;
        this.currentPlayer = o;
        this.count = 0;
    }

    public char[][] getTable() {
        return table;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public Player getO() {
        return o;
    }

    public Player getX() {
        return x;
    }

    public int getCount() {
        return count;
    }

    private void switchPlayer() {
        if (currentPlayer == o) {
            currentPlayer = x;
        } else {
            currentPlayer = o;
        }
    }

    public boolean isWin() {
        return Win;
    }

    public boolean isDraw() {
        return draw;
    }

    public boolean setRowCol(int row, int col) {
        if(isWin()||isDraw())return false;
        this.row = row;
        this.col = col;
        if (row > 3 || col > 3 || row < 1 || col < 1 || col < 1) {
            return false;
        }
        if (table[row - 1][col - 1] != '-') {
            return false;
        }
        table[row - 1][col - 1] = currentPlayer.getSymbol();
        if (checkWin(row, col)) {
            UpdateStaT();
            this.Win = true;
            return true;
        }
        if (checkDraw()) {
            o.Draw();
            x.Draw();
            this.draw = true;
            return true;
        }
        count++;
        switchPlayer();
        return true;
    }

    private void UpdateStaT() {
        if(this.currentPlayer==o){
            o.Win();
            x.Lose();
        }else{
            x.Win();
            o.Lose();
        }
    }

    public boolean checkDraw() {
        if (count == 8) {
            return true;
        }
        return false;
    }

    public boolean checkWin(int row, int col) {
        if (checkVertical()) {
            return true;
        } else if (checkHorizontal()) {
            return true;
        } else if (checkDia()) {
            return true;
        }

        return false;

    }

    public boolean checkVertical() {
        for (int r = 0; r < table.length; r++) {
            if (table[r][col - 1] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;

    }

    public boolean checkHorizontal() {
        for (int c = 0; c < table.length; c++) {

            if (table[row - 1][c] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;

    }

    public boolean checkDia() {
        if (checkX1()) {
            return true;

        } else if (checkX2()) {
            return true;
        }
        return false;
    }

    public boolean checkX1() {
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkX2() {
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

}
