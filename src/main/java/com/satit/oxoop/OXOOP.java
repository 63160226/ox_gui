/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.satit.oxoop;

/**
 *
 * @author satit
 */
public class OXOOP {

    public static void main(String[] args) {
        GAME game = new GAME();
        game.showWelcome();
        game.newBoard();
        while(true){
            game.ShowTable();
            game.showTurn();
            game.inputRowcol();
            if(game.isfnish()){
            game.ShowTable();
            game.showResult();
            game.showStat();
            game.newBoard();
            }
            }
    }
}
